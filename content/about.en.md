+++
title = "About Victor"
slug = "about"
thumbnail = "images/tn.png"
description = "about"
+++

---------------------------


PHP Developer, I have been working for 4 years with the Laravel framework in most of my projects. Currently working as GCP Cloud Admin at [Veus Technology](http://site.veusserver.com/).

In my free time I try to focus on my personal projects and articles.

* [Baleia Rosa](https://github.com/victorhugorch/baleia-rosa) - Twitter Bot to help people with depression.
* [Xplain Shell](https://github.com/victorhugorch/xplainshell-bot) - Telegram Bot explaining the meaning of a shell command written by the user.

Thanks for reading!
