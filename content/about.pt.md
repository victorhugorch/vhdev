+++
title = "Sobre Victor"
slug = "about"
thumbnail = "images/tn.png"
description = "about"
+++

---------------------------

Desenvolvedor PHP, trabalho há 4 anos com o framework Laravel na maioria dos meus projetos. Atualmente trabalhando como GCP Cloud Admin na [Veus Technology](http://site.veusserver.com/).

No meu tempo livre tento focar nos meus projetos pessoais e artigos.

* [Baleia Rosa](https://github.com/victorhugorch/baleia-rosa) - Twitter Bot para ajudar pessoas com depressao.
* [Xplain Shell](https://github.com/victorhugorch/xplainshell-bot) - Telegram Bot que explica o funcionamento de um comando Shell.

Obrigado por ler!
